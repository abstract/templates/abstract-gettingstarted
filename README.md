# Abstract-GettingStarted

This is the getting started template that any Abstract OS project could start with. This template is designed to get a developer up to speed quickly with building an Abstract OS Application!


### To Build This Sample!

Simply run the commands

```
meson build --prefix=/usr
cd build
ninja
```