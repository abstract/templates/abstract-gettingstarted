public class MyApp : Abstract.Application {
    public MyApp() {

    }

    protected override void activate(){
        var main_window = new Abstract.ApplicationWindow(this){
            default_height = 300,
            default_width = 300,
            title = "Hello World"
        };

        var helloworld_btn = new Gtk.Button() {
            label = "Hello World!",
            margin_bottom = 12,
            margin_top = 12,
            margin_start = 12,
            margin_end = 12
        };

        helloworld_btn.clicked.connect (() => {
            if(helloworld_btn.label == "Hello World!") {
                helloworld_btn.label = "Hello User!";
            } else {
                helloworld_btn.label = "Hello World!";
            }
            
        });

        main_window.set_child(helloworld_btn);
        main_window.show();
    }

    public static int main(string[] args) {
        return new MyApp().run(args);
    }
}
